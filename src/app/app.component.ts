import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MyFormControl } from './my-form-control';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'my-sandbox';

    parentFormGroup: FormGroup;
    ngOnInit() {
        this.parentFormGroup = new FormGroup({
            field1a: new MyFormControl(),
            field2a: new MyFormControl(),
            field3a: new MyFormControl(),
            field1b: new MyFormControl(),
            field2b: new MyFormControl(),
            field3b: new MyFormControl()
        });

        (<MyFormControl>this.parentFormGroup.get('field1a')).onFormValueChange.subscribe((value)=>{
            console.log('Main component get new value of field1a: ' + value);
            let formGroupValue = this.parentFormGroup.get('field1a').value
            console.log('In form group value field1a is: ' + formGroupValue) ;
            this.parentFormGroup.get('field1b').setValue(formGroupValue);
        });
        (<MyFormControl>this.parentFormGroup.get('field2a')).onFormValueChange.subscribe((value)=>{
            console.log('Main component get new value of field2a: ' + value);
            let formGroupValue = this.parentFormGroup.get('field2a').value
            console.log('In form group value field2a is: ' + formGroupValue) ;
            this.parentFormGroup.get('field2b').setValue(formGroupValue);
        });
        (<MyFormControl>this.parentFormGroup.get('field3a')).onFormValueChange.subscribe((value)=>{
            console.log('Main component get new value of field3a: ' + value);
            let formGroupValue = this.parentFormGroup.get('field3a').value
            console.log('In form group value field3a is: ' + formGroupValue) ;
            this.parentFormGroup.get('field3b').setValue(formGroupValue);
        });
    }


    setValueFromSecondControl(){
        let field2Value = this.parentFormGroup.get('field2a').value;
        (<MyFormControl>this.parentFormGroup.get('field1a')).setControlValue(field2Value, true);
    }



}
