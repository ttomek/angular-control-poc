import { FormGroup, FormControl } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { Component } from '@angular/core';

export class MyFormControl extends FormControl {

	onFormValueChange = new EventEmitter();

//używając tej metody możemy ustawić wartość w formGroupie i w zależności od shouldEmitEvent emitować onFormValueChange
	setControlValue(value, shouldEmitEvent? : boolean){
		this.setValue(value, {emitEvent: true})
		if(shouldEmitEvent){
		//w komponentach nasłuchujemy na tym evencie
			this.onFormValueChange.emit(value);
		}
	}
}
