import { Component, OnInit, Input,  ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MyFormControl } from '../my-form-control';
@Component({
    selector: 'app-simple-input',
    templateUrl: './simple-input.component.html',
    styleUrls: ['./simple-input.component.scss']
})
export class SimpleInputComponent implements OnInit {
    @Input('label') label: string;
    @Input('parentFormGroup') parentFormGroup: FormGroup;
    @Input('onInputEmitter') onInputEmitter: EventEmitter<any> = new EventEmitter();
    @Input('controlName') controlName: string;
    @ViewChild('displayField', {static: true}) displayField: ElementRef;

    constructor() { }

    ngOnInit() {
    // to jest odpalane przez my=form-control.ts linia this.setValue(value, {emitEvent: true})
        this.parentFormGroup.get(this.controlName).valueChanges.subscribe( value=> {
            this.displayField.nativeElement.value = value;
            console.log('Simple component new value for ' + this.controlName + ' is ' + value);
         });
    }

    onInput(){
        let currentInputValue = this.displayField.nativeElement.value;
        //ustawia wartość w formGroup, bez emitowania
        this.parentFormGroup.get(this.controlName).setValue(currentInputValue, {emitEvent: false});
        // ewentualnie gdyby ktość potrzebował takiego emitowania to można emitować eventEmitter np. onInputEmitter.emit(currentInputValue)
        this.onInputEmitter.emit(currentInputValue);
    }

    //w tej kontrolce onBlur() jest zdarzeniem, które odpala się gdy użytkownik skończył wpisywać, dla innych np. select mogą to być inne zdarzenia
    // chce zwrocic uwage ze jest to zalezne od typu
    onBlur(){
        // w tym emitowanie wartości z formGroup, można nasłuchiwać tego eventu zapinająć się np.
        // (<MyFormControl>this.parentFormGroup.get(this.controlName)).onFormValueChange.subscribe(()=>{})
        let value = this.parentFormGroup.get(this.controlName).value;
        (<MyFormControl>this.parentFormGroup.get(this.controlName)).onFormValueChange.emit(value);
    }

}
