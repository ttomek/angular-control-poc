import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SimpleInputComponent } from './simple-input/simple-input.component';
import { MyFormControl } from './my-form-control';

@NgModule({
    declarations: [
        AppComponent,
        SimpleInputComponent,
        
    ],
    imports: [
        ReactiveFormsModule,
        BrowserModule,
        AppRoutingModule,
        
    ],
    providers: [MyFormControl],
    bootstrap: [AppComponent]
})
export class AppModule { }
